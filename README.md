# .Net Core Api脚手架，拿来即用

## 介绍
本脚手架支持快速开发.Net Core Api接口，实现前后端分离，也支持开发多租户系统。
## 软件架构
使用Asp.net core 3.1+EF Core+JWT+Autofac+Swagger+StackExchange.Redis/csredis+Quartz定时器+Mustachio模板引擎代码生成器   
其他：ChakraCore.NET JavaScript引擎

## 使用教程
#### 1. [快速开发](doc/Base.md)
#### 2. [权限控制](doc/Authority.md)
#### 3. [登录与请求、获取用户信息](doc/Login.md)
#### 4. [查询过滤器](doc/Query.md)
## 目录结构
  Model：模型层  
  DAL：数据访问层  
  BLL：业务逻辑层 
  Web：控制器层  
  ImCore：即时聊天websocket核心
## 参考文档
[码云Markdown语法](https://gitee.com/oschina/team-osc/blob/master/markdown.md)  
[码云commonmark语法](https://commonmark.org/help/)

